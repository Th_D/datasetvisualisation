#ifndef FOS
#define FOS


#include <map>
#include <string>
#include <set>


class Fos_datas
{
private:

	struct comp_fos
	{
		template<typename T>
		bool operator()(const T& l, const T& r) const
		{
			if (l.second != r.second)
				return l.second > r.second;

			return l.first < r.first;
		}
	};

	std::map<int, std::map<std::string, int>*> datas;
	
	//std::map<int YEAR, std::map<std::string FOS, int APPARITIONS>> datas;

public:
	Fos_datas();
	void addVal(int, std::string);
	void sort();
	int min_year;
	std::map<int, std::set<std::pair<std::string, int>, Fos_datas::comp_fos>*> sorted_data_per_year;
	

};

#endif