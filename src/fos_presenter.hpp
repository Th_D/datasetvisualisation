#ifndef FOS_P
#define FOS_P


#include <map>
#include <string>
#include <vector>

#include "fos_datas.hpp"

#define NB_FOS_TO_DISP 300
#define FOS_STEP_X 3
#define FOS_RAD 1

class Fos_presenter
{
private:

	Fos_datas* fos_datas;
	int min_year;
	const int max_year = 2020;

	struct val_peer_2years
	{
		int val_N = 0;
		int val_N_p1 = 0;
	};

	struct val_at_moment
	{
		std::string fos;
		double val;


		val_at_moment(std::string lbl, int v)
		{
			fos = lbl;
			val = v;
		}

		bool operator <(const val_at_moment& vam) const
		{
			return (val > vam.val);
		}
	};

	std::map<std::string, val_peer_2years> values;
	std::set<val_at_moment> ordered_val_at_moment;

public:
	Fos_presenter(int min_y, Fos_datas*);
	void disp_year(double year);
	

	

};

#endif