#ifndef UTILS
#define UTILS

#include <GLFW/glfw3.h>
#include <math.h>
#include <corecrt_math_defines.h>
#include "parser.hpp"
#include "glm/glm/glm.hpp"
#include <glm/glm/ext/matrix_transform.hpp>
#include <iostream>
#include <windows.h>
#include <gl/Glu.h>
#include "glut-3.7.6-bin32/glut.h"


#define DATA_PATH (ExePath() + std::string("../../../../data/dblp.v11.csv"))

void draw_axis()
{
    glPushMatrix();
    glBegin(GL_LINES);
    // draw line for x axis
    glColor4f(1.0, 0.0, 0.0, 1.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(1.0, 0.0, 0.0);
    // draw line for y axis
    glColor4f(0.0, 1.0, 0.0, 1.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 1.0, 0.0);
    // draw line for Z axis
    glColor4f(0.0, 0.0, 1.0, 1.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 1.0);
    glEnd();
    glPopMatrix();
}

std::string ExePath() {
    char buffer[MAX_PATH];
    GetModuleFileName(NULL, buffer, MAX_PATH);
    std::string::size_type pos = std::string(buffer).find_last_of("\\/");
    return std::string(buffer).substr(0, pos);
}



#endif