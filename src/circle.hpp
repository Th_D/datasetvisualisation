#ifndef CIRCLE
#define CIRCLE

#include <GLFW/glfw3.h>
#include <math.h>
#include <corecrt_math_defines.h>
#include "parser.hpp"
#include "glm/glm/glm.hpp"
#include <glm/glm/ext/matrix_transform.hpp>
#include <iostream>
#include <windows.h>
#include <gl/Glu.h>
#include "glut-3.7.6-bin32/glut.h"


#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480
#define NBOFVERTICES 360

class Circle
{
private:

    //position of the center
    float pos_x;
    float pos_y;
    float pos_z;
    float radius;

public:

    Circle(float r=2, float x=0, float y=0, float z=0)
    {
        radius = r;

        pos_x = x;
        pos_y = y;
        pos_z = z;
    }

    void draw()
    {
        glBegin(GL_TRIANGLE_FAN);
        glTranslated(pos_x, pos_y, pos_z);
        glVertex3f(pos_x, pos_y, pos_z);  // center
        for (double i = 0.; i <= 2 * M_PI+1; i += M_PI / NBOFVERTICES)
            glVertex3f(radius * cos(i) + pos_x, pos_y, radius * sin(i) + pos_z);
        glEnd();
    }

};



/*
void drawCircle(GLfloat x, GLfloat y, GLfloat z, GLfloat radius, GLint numberOfSides)
{
    GLint const numberOfVertices = 60;

    GLfloat doublePi = 2.0f * 3.1416;

    GLfloat circleVerticesX[numberOfVertices];
    GLfloat circleVerticesY[numberOfVertices];
    GLfloat circleVerticesZ[numberOfVertices];

    //circleVerticesX[0] = x;
    //circleVerticesY[0] = y;
    //circleVerticesZ[0] = z;

    for (int i = 0; i < numberOfVertices; i++)
    {
        circleVerticesX[i] = x + (radius * cos(i * doublePi / numberOfSides));
        circleVerticesY[i] = y + (radius * sin(i * doublePi / numberOfSides));
        circleVerticesZ[i] = z;

    }

    GLfloat allCircleVertices[numberOfVertices * 3];

    for (int i = 0; i < numberOfVertices; i++)
    {
        allCircleVertices[i * 3] = circleVerticesX[i];
        allCircleVertices[(i * 3) + 1] = circleVerticesY[i];
        allCircleVertices[(i * 3) + 2] = circleVerticesZ[i];
    }


    glEnableClientState(GL_VERTEX_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, allCircleVertices);
    glDrawArrays(GL_LINE_STRIP, 0, numberOfVertices);



    glDisableClientState(GL_VERTEX_ARRAY);
}

void drawCircleWithTriangles(GLfloat x, GLfloat y, GLfloat z, GLfloat radius, GLint nbOfSides) {
    const GLint nbOfVertices = NBOFVERTICES + 2;
    GLfloat doublePi = 2.0f * M_PI;
    GLfloat circleVerticesX[nbOfVertices];
    GLfloat circleVerticesY[nbOfVertices];
    GLfloat circleVerticesZ[nbOfVertices];

    circleVerticesX[0] = x;
    circleVerticesY[0] = y;
    circleVerticesZ[0] = z;

    for (int i = 1; i < nbOfVertices; i++)
    {
        circleVerticesX[i] = x + (radius * cos(i * doublePi / nbOfSides));
        circleVerticesY[i] = x + (radius * sin(i * doublePi / nbOfSides));
        circleVerticesZ[i] = z;
    }

    GLfloat allCircleVertices[nbOfVertices * 3];
    for (int i = 0; i < nbOfVertices; i++)
    {
        allCircleVertices[i * 3] = circleVerticesX[i];
        allCircleVertices[(i * 3) + 1] = circleVerticesY[i];
        allCircleVertices[(i * 3) + 2] = circleVerticesZ[i];
    }
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, allCircleVertices);
    glDrawArrays(GL_TRIANGLE_FAN, 0, nbOfVertices);
    glDisableClientState(GL_VERTEX_ARRAY);
}
*/

#endif