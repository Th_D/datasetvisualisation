#ifndef PARSER
#define PARSER

#include <string>
#include "fos_datas.hpp"

class Parser
{
private:
	std::string file;

public:
	Parser(std::string, Fos_datas*);
	
};

#endif