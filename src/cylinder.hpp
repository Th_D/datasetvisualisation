#ifndef CYLINDER
#define CYLINDER

#include <GLFW/glfw3.h>
#include <math.h>
#include <corecrt_math_defines.h>
#include "parser.hpp"
#include "glm/glm/glm.hpp"
#include <glm/glm/ext/matrix_transform.hpp>
#include <iostream>
#include <windows.h>
#include <gl/Glu.h>
#include <string>
#include "glut-3.7.6-bin32/glut.h"
#include <freeglut.h>

#include "circle.hpp"





class Cylinder
{
private:

    float height;
    float radius;

    //position of the center of the bottom circle
    float pos_x;
    float pos_y;
    float pos_z;

    std::string label;

    Circle* circle_bottom;
    Circle* circle_top;



public:

    Cylinder(float r = 1, float h = 3, float x = 0, float y = 0, float z = 0, std::string lbl="")
    {
        radius = r;
        height = h;

        pos_x = x;
        pos_y = y;
        pos_z = z;

        label = lbl;

        circle_bottom = new Circle(r,x,y,z);
        circle_top = new Circle(r, x, y+h, z);
    }

    void draw()
    {


        glPushMatrix();
        glTranslated(pos_x, pos_y, pos_z); //translate the cylinder

        /* top triangle */
        circle_top->draw();
        circle_bottom->draw();

        /* middle tube */
        glBegin(GL_QUAD_STRIP);
        for (float i = 0; i <= 2 * M_PI; i += M_PI / NBOFVERTICES)
        {
            glVertex3f((radius * cos(i)) + pos_x, pos_y, (radius * sin(i)) + pos_z);
            glVertex3f((radius * cos(i)) + pos_x, height+ pos_y, (radius * sin(i)) +pos_z);
        }
        /* close the loop back to zero degrees */

        glVertex3f(radius + pos_x, pos_y, pos_z);
        glVertex3f(radius + pos_x, height + pos_y, +pos_z);
        glEnd();
        glPopMatrix();

        glPushMatrix();
        //glTranslated(pos_x, pos_y - 3., pos_z); //translate the text
        glRasterPos3f(pos_x * 2, pos_y - 3., pos_z);
        glScalef(1, 1, 1);
        glutBitmapString(GLUT_BITMAP_HELVETICA_12, (unsigned char*)label.c_str());
        
        glRasterPos3f(pos_x * 2 -1 , (height + pos_y + 1.), pos_z-2);
       // glScalef(0.2, 0.2, 0.2);
        char buff[11];
        sprintf(buff, "%d", (int)height+1);
        std::cout << buff << std::endl;
        glutBitmapString(GLUT_BITMAP_HELVETICA_18, (unsigned char*)buff);
        glPopMatrix();
    }
};

#endif