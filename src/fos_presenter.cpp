


#include <iostream>
#include <map>
#include <set>

#include "fos_presenter.hpp"
#include "cylinder.hpp"



Fos_presenter::Fos_presenter(int min_y, Fos_datas* datas)
{
	min_year = min_y;
	fos_datas = datas;

	//std::set<std::string>::iterator setIt = setOfStr.begin();


	

};

void Fos_presenter::disp_year(double year)
{
	glPushMatrix();
	//glTranslated(pos_x, pos_y - 3., pos_z); //translate the text
	glRasterPos3f(0, -5., 0);

	char buff[7];
	sprintf(buff, "%d", (int)year);

	glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (unsigned char*)buff);
	glPopMatrix();

	//std::cout << "DISP YEAR" << year << std::endl;
	values.clear();
	ordered_val_at_moment.clear();
	if (year >= min_year && year <= max_year)
	{
		if (fos_datas->sorted_data_per_year[year] && fos_datas->sorted_data_per_year[year]->size() > 0)
		{
			std::set<std::pair<std::string, int>>::iterator setIt = fos_datas->sorted_data_per_year[year]->begin();
			float pos_x = 0;

			for (int i = 0; (i < NB_FOS_TO_DISP && i < fos_datas->sorted_data_per_year[year]->size()); i++)
			{
				values[(*setIt).first].val_N = (*setIt).second;
				setIt++;
			}
		}
		if (fos_datas->sorted_data_per_year[year+1] && fos_datas->sorted_data_per_year[year+1]->size() > 0)
		{
			std::set<std::pair<std::string, int>>::iterator setIt = fos_datas->sorted_data_per_year[year+1]->begin();
			float pos_x = 0;

			for (int i = 0; (i < NB_FOS_TO_DISP && i < fos_datas->sorted_data_per_year[year+1]->size()); i++)
			{
				if(values.find((*setIt).first) != values.end())
					values[(*setIt).first].val_N_p1 = (*setIt).second;
				setIt++;
			}
		}

		
		//std::cout << " SIZE = " << values.size() << std::endl;
		
		for (const auto& kv : values) {
			double val = ((kv.second.val_N * (1. - (year - floor(year))))
					   +(kv.second.val_N_p1 * (year - floor(year))));

			ordered_val_at_moment.insert(val_at_moment(kv.first, val));
		}

		int pos_x = 0;
		for (const auto& kv : ordered_val_at_moment) {

			Cylinder fos_cylinder(FOS_RAD, kv.val/1.5, pos_x, -3, -3, kv.fos);
			fos_cylinder.draw();
			pos_x += FOS_STEP_X;
			//std::cout << kv.fos << " has value " << kv.val << std::endl;
		}


	}
	else
	{
		std::cerr << "Error: try to disp a wrong year [" << year << std::endl;
	}

};

