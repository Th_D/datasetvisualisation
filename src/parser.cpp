
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "parser.hpp"

std::vector<std::string> split(std::string str, std::string token) {
    std::vector<std::string>result;
    while (str.size()) {
        std::size_t index = str.find(token);
        if (index != std::string::npos) {
            result.push_back(str.substr(0, index));
            str = str.substr(index + token.size());
            if (str.size() == 0)result.push_back(str);
        }
        else {
            result.push_back(str);
            str = "";
        }
    }
    return result;
}

Parser::Parser(std::string file, Fos_datas *datas)
{
    std::cout << file << std::endl;
	std::string line="start ";
	std::ifstream filestream;
	filestream.open(file);
	int cpt = 0; //tmptest
    datas->min_year = 999999;
	while (filestream.is_open() && !line.empty()) //&& cpt++<6000) // To get all the lines.
	{
		std::getline(filestream, line);
        if (line.empty())
            break;
        std::string year_str;

        int year;
        try
        {
            year_str = split(line, ",")[3];
            year_str.erase(std::remove(year_str.begin(), year_str.end(), '"'), year_str.end());
            year = std::stoi(year_str);
            if (year < datas->min_year) {
                datas->min_year = year;
                //std::cout << datas->min_year << std::endl;
            }
        }
        catch (...)
        {
            year = 0;
            //std::cout << "Bad input: std::invalid_argument thrown" << '\n';
        }
        
        //std::cout << split(line, ",")[3]<< "TOTO:" << year << std::endl;
        std::vector<std::string> line_fos;
        line_fos = split(split(line, ",")[5], ";");
        for (auto fos : line_fos)
        {
            fos.erase(std::remove(fos.begin(), fos.end(), '"'), fos.end());
            if(split(fos, ":")[0] != "computer science")
                datas->addVal(year, split(fos, ":")[0]);
            //std::cout << fos << std::endl;
        }
		    
        //std::cout <<cpt<< std::endl;
    }
    std::cout << cpt << " lines saved" << std::endl;
	filestream.close();
}

