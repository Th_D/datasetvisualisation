


#include <iostream>
#include <map>
#include <set>

#include "fos_datas.hpp"


Fos_datas::Fos_datas()
{}

void Fos_datas::addVal(int year, std::string fos)
{
	//std::cout << "ADD " << fos << " in " << year << std::endl;
	if (!datas.insert(std::make_pair(year, new std::map<std::string, int>)).second) {
		//year already present
	}
	
	if (!datas[year]->insert(std::make_pair(fos, 1)).second) //set 1 if not present yet
	{
		//fos already present for this year, so increment
		(*datas[year])[fos]++;
	}
}



void Fos_datas::sort()
{

	for (int i = min_year; i < 2020; i++)
	{
		// input map
		if (datas[i]) // if not null
		{
			std::map<std::string, int>* map = datas[i];

			// create an empty vector of pairs
			sorted_data_per_year[i] = new std::set<std::pair<std::string, int>, comp_fos>(map->begin(), map->end());
			//for (auto y : *sorted_data_per_year[i]) {
			//	std::cout << '{' << y.first << "," << y.second << '}' << '\n';
			//}
			// print the vector
		}
		else
		{
			datas[i] = new std::map<std::string, int>();
		}
	}
	
	
}

