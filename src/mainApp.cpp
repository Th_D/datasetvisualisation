

#include "circle.hpp"
#include "cylinder.hpp"
#include "utils.hpp"
#include "fos_presenter.hpp"



//angle of rotation
float xpos = 0, ypos = 0, zpos = 0, xrot = 0, yrot = 0; // angle = 0.0;

float lastx, lasty;


bool running_status = true;
double running_time = 0;
double old = 0;

Cylinder* cylinderTest = new Cylinder(3,3,-3,-3,-3);
Circle* circleTest = new Circle(3,-13,3);

Fos_datas* fos_data = new Fos_datas();
Fos_presenter presenter(1900, fos_data);





//draw the cube
void cube(void) {
   /* for (int i = 0; i < 10; i++)
    {
        glPushMatrix();
        glTranslated(-positionx[i + 1] * 10, 0, -positionz[i + 1] * 10); //translate the cube
        glutSolidCube(2); //draw the cube
        glPopMatrix();
    }*/
}

void init(void) {
    
    std::cout << "START REAL PARSING" << std::endl;
    Parser* parser = new Parser(DATA_PATH, fos_data);
    fos_data->sort();
    std::cout << "PARSING FINISHED" << std::endl;
    char buffer[MAX_PATH];
    GetModuleFileName(NULL, buffer, MAX_PATH);
    //std::cout << buffer << std::endl;
    
    
}

void enable(void) {
    glEnable(GL_DEPTH_TEST); //enable the depth testing
    glEnable(GL_LIGHTING); //enable the lighting
    glEnable(GL_LIGHT0); //enable LIGHT0, our Diffuse Light
    glShadeModel(GL_FLAT); //set the shader to smooth shader

}

void camera(void) {
    glRotatef(xrot, 1.0, 0.0, 0.0);  //rotate our camera on the x-axis (left and right)
    glRotatef(yrot, 0.0, 1.0, 0.0);  //rotate our camera on the y-axis (up and down)
    glTranslated(-xpos, -ypos, -zpos); //translate the screen to the position of our camera
}

void display(void) {
    double timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
    double deltaTime = timeSinceStart - old;
    old = timeSinceStart;
    if (running_status)
    {
        running_time += deltaTime;
    }
    //std::cout << (running_time / 1000. / 1.5) << std::endl;
    glClearColor(0.0, 0.0, 0.0, 1.0); //clear the screen to black
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear the color buffer and the depth buffer
    glLoadIdentity();
    camera();
    enable();

    //draw_axis();

//    std::cout<< "xpos"<< xpos <<"ypos"<< ypos <<"zpos"<< zpos <<"xrot"<< xrot <<"yrot"<< yrot <<std::endl;

    //cylinderTest->draw();
    //circleTest->draw();
    
    presenter.disp_year(fos_data->min_year + (running_time / 1000. / 1.5));
    
    //std::cout << int(running_time /1000.) << std::endl;
    cube(); //call the cube drawing function
    glutSwapBuffers(); //swap the buffers
    //angle++; //increase the angle
}

void reshape(int w, int h) {
    glViewport(0, 0, (GLsizei)w, (GLsizei)h); //set the viewport to the current window specifications
    glMatrixMode(GL_PROJECTION); //set the matrix to projection
    glLoadIdentity();
    gluPerspective(60, (GLfloat)w / (GLfloat)h, 1.0, 1000.0); //set the perspective (angle of sight, width, height, depth)
    glMatrixMode(GL_MODELVIEW); //set the matrix back to model
}

void keyboard(unsigned char key, int x, int y) {
    /*if (key == 'q')
    {
        xrot += 1;
        if (xrot > 360) xrot -= 360;
    }

    if (key == 'z')
    {
        xrot -= 1;
        if (xrot < -360) xrot += 360;
    }*/

    if (key == 'w' || key == 'z' || key == 'W' || key == 'Z')
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        xpos += float(sin(yrotrad));
        zpos -= float(cos(yrotrad));
        ypos -= float(sin(xrotrad));
    }

    if (key == 's' || key == 'S')
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        xpos -= float(sin(yrotrad));
        zpos += float(cos(yrotrad));
        ypos += float(sin(xrotrad));
    }

    if (key == 'd' || key == 'D')
    {
        float yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xpos += float(cos(yrotrad)) * 0.4;
        zpos += float(sin(yrotrad)) * 0.4;
    }

    if (key == 'a' || key == 'q' || key == 'A' || key == 'Q')
    {
        float yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xpos -= float(cos(yrotrad)) * 0.4;
        zpos -= float(sin(yrotrad)) * 0.4;
    }

    if (key == ' ')
    {
        running_status = !running_status;
    }

    if (key == '1' || key == '&')
    {
        xpos = 5;
        ypos = 25;
        zpos = 24;
        xrot = 35;
        yrot = 22;
    }

    if (key == '2' || key == '�')
    {
        xpos = -26;
        ypos = 80;
        zpos = 80;
        xrot = 20;
        yrot = 40;
    }

    if (key == 'n' || key == 'N')
    {
        running_time += 1500;
        if (running_time / 1500 > 2021)
            running_time = 1500 * 2021;
    }

    if (key == 'b' || key == 'B')
    {
        running_time -= 1500;
        if (running_time < 0)
            running_time = 0;
    }

    if (key == 27) //escape
    {
        exit(0);
    }
}

void mouseMovement(int x, int y) {
    int diffx = x - lastx; //check the difference between the current x and the last x position
    int diffy = y - lasty; //check the difference between the current y and the last y position
    lastx = x; //set lastx to the current x position
    lasty = y; //set lasty to the current y position
    xrot += (float)diffy; //set the xrot to xrot with the addition of the difference in the y position
    yrot += (float)diffx;    //set the xrot to yrot with the addition of the difference in the x position
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(1000, 800);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("dataset visualisation");
    init();
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutPassiveMotionFunc(mouseMovement); //check for mouse movement
    glutKeyboardFunc(keyboard);
    glutMainLoop();

    
    return 0;
}


